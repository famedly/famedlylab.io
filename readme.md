# Description

We use this project to forward human users to our appstore entries. The code is deployed under this url: https://famedly.gitlab.io/.

This url can be used to point human users to our app-store entires. Based on the user agent, we suggest the user to open the Apple AppStore, or Android Appstore.
